import React, { Component, Fragment } from 'react';
import _ from 'lodash';
import fp from 'lodash/fp';

import TextInputSUI from './TextInput';
import SelectBox from './SelectBox';

const printWarn = console.warn.bind(console,'warn => ');

const componentFromUI = (ui = _.noop, name='') => {
  return class extends Component {
    static displayName = name;
    render(){
     return ui(this.props)
    };
  }
};

const sectionCreator =  function sectionCreator( name = '') {
   return class extends React.Component {
     static displayName = name;
     render() {
       const {
        id='',
        title='',
        subtitle=''
       } = this.props;

       return (
        <section data-qa={id}>
         <h3>{title}</h3>
        {
            subtitle && <p>{subtitle}</p>
        }
         <hr className="divider" />
          {
            this.props.children 
          }
        </section>
       ) 
     }
   }
}

const DefaultComp = componentFromUI(()=> <div />, 'DefaultElm');

const TextInput = componentFromUI((props={}) => {
    return (
        <Fragment>
         <TextInputSUI {...props} />
         {
         props.desc && 
          <span className="input__descriptor input__descriptor-unit">
            {props.desc}
          </span>
        }
        </Fragment>
    );
}, 'TextInputWithDescriptor')

const ElementTypeSelector = componentFromUI((props = {}) => {
    const {
     type
    } = props;

    const elems = {
     TextInput,
     SelectBox,
     DefaultComp,
    };

   const AuxEl = _.get(elems, type, elems.DefaultComp);
   return <AuxEl {...props} />;

}, 'ElementTypeSelector');

const setOnChangeHandler = (fn=_.noop) => 
  (props) => fp.merge(props, {onChange: fn(fp.path('onChange', props))})

    
const CreateListOfFields = componentFromUI((props = {}) =>{
    const elmsList = _.get(props, 'elements',  []);
    const onChangeHandler = _.get(props, 'onChangeHandler', x => x);
    const getPropsForComp = setOnChangeHandler(onChangeHandler);

    return (<ul>
            {
            elmsList.map(elm =>{
                
                const elementData = fp.path(elm, props);

                if(_.isNil(elementData)){
                    printWarn(`data for ${elm} is undefined`);
                    return null;
                }
              
             return (
                 <li key={elm}>
                    <ElementTypeSelector  
                        {...getPropsForComp(elementData)} 
                    />
                </li>)
             })
            }
            </ul>);
}, 'CreateListOfFields');


const SectionsMapper = componentFromUI(({
     sections = [], 
     properties={},
     onChangeHandler=_.noop}) => {

    return sections.map( sec => {
        const section = fp.path(sec , properties);
        const name = _.capitalize(_.get(section, 'id', ''));

        if (_.isNil(section)){
          printWarn(`section: ${sec} is undefined`);
          return <div key={sec}/>
         }

        const FormSection = sectionCreator(name);

        return (
           <FormSection key={sec} {...section}>
               <CreateListOfFields 
                 onChangeHandler={onChangeHandler}
                 elements={section.elements}
                 {...section.properties} />
           </FormSection>
           );
    });
    
}, 'SectionsMapper');


export default function FormCreator (React) {
     return class extends React.Component {
      static displayName = 'FormComponent';
      
      render() {
        
        const {
            onChange,
            schema,
        } = this.props;

        return (
         <form id={this.props.myName}>
            <fieldset>
                <SectionsMapper 
                    {...schema} 
                    onChangeHandler={onChange}/>
           </fieldset>
         </form>
        );
      }
     }
};

