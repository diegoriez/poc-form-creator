import React, { Component } from 'react';

import '../node_modules/aem-dso-patternlab/build/styles.min.css';
import '../node_modules/aem-dso-patternlab/pattern-color-css/color.css';

import './App.css';

import FormCreator from './FormCreator';

const print =  console.log.bind(console,'log => ');

const onChangeHandler = (reference='') => {
    return function onChange (e = {}) {
      const callbacksHandlers = {

      onLocationAddOn:  x => {
        print('onLocationAddOn', x);
      },

      onChangePowerInKwHandler: x => {
        print('power in kw meter: ', x);
      },

      onChangeExpectedYearlyConsumptionInKw: x => {
        print('consumption in kw: ', x);
      },

      default: x => x
     };

      const action = callbacksHandlers[reference] || callbacksHandlers.default; 
      action(e.target.value);
    }
};

const schema = {
  "title": "test form",
  "description": "",
  "type": "object",
  "required": [],
  "properties":{
   "meterData": {
     "id":"meter-data", // the key
     "title":"Zählerdaten",
     "subtitle":"",
     "elements": [
       "powerInKw",
       "expectedYearlyConsumptionInKW",
       "meterType",
       "locationAddOn", 
       "meterLocationKey",
       ],
     "properties":{
        "locationAddOn":{
          "errorMessage":"",
          "hasError":false,
          "disabled": false,
          "id":"location-add-on",
          "isMandatory": true,
          "qaId":"location-add-on",
          "label":"Standortzusatz",
          "name":"location-add-on",
          "type": "TextInput",
          "dataType": "string",
          "value": "",
          "onChange": "onLocationAddOn"
        },
        "meterLocationKey": {
          "dataType":"string",
          "disabled": true,
          "label": "",
          "id":"meter-key",
          "qaId":"meter-key",
          "type":"SelectBox",
          "value":"1234",
          "options": [
             {"key":"Zähleranschlußsäule", "value":"1234"}
            ]
        },
        "meterType":{
          "dataType":"string",
          "disabled": true,
          "label": "",
          "id":"meter-type",
          "qaId":"meter-type",
          "type":"SelectBox",
          "value":"opt-0",
          "options": [
             {"key":"Drehstrom, 1Rtg., Eintarif(bis 40kW / 60A)", "value":"opt-0"}
            ]
        },
        "expectedYearlyConsumptionInKW": {
          "desc": "KWh",
          "dataType": "number",
          "id":"expectedYearlyConsumptionInKw",
          "isMandatory": true,
          "qaId":"expetcted-yearly-consumption-in-kw",
          "label": "Erwarteter Jahresverbrauch",
          "name":"expetcted-yearly-consumption-in-kw",
          "type": "TextInput",
          "value":"",
          "onChange": "onChangeExpectedYearlyConsumptionInKw"
        },
        "powerInKw":{
          "dataType": "number",
          "desc": "Kw",
          "id":"power-in-kw",
          "isMandatory": true,
          "qaId":"power-in-kw-meter",
          "label": "Leistung",
          "name":"power-in-kw-meter",
          "type": "TextInput",
          "value":"",
          "onChange": "onChangePowerInKwHandler"
        },
     }
   },
  },
   "sections": ["meterData"]
};

const formData = {
  "firstName": "Peter",
  "secondName": "Pan"
};

const InstallerData = FormCreator(React);

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Avacon POC</h1>
        </header>
        <div className="form-container">
            <InstallerData 
                myName="installer"
                schema={schema}
                formData={formData}
                onChange={onChangeHandler}
            />
        </div>
      </div>
    );
  }
}

export default App;
