import PropTypes from 'prop-types';
import React from 'react';
import {TextInput as TextInputSUI, InputState} from 'react-shared-ui-next';

class TextInput extends React.Component {

  render() {
    const {
      id,
      qaId,
      disabled,
      isMandatory,
      hasError,
      maxLength,
      value,
    } = this.props;
    

    return (
      <TextInputSUI
        {...this.props}
        disabled={disabled}
        id={id || qaId}
        qaId={qaId || id}
        required={isMandatory}
        inputState={hasError ? InputState.ERROR : InputState.NONE}
        value={String(value || '').substring(0, (maxLength || Infinity))}
      />
    );
  }
}

TextInput.propTypes = {
  qaId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  onCopy: PropTypes.func,
  onPaste: PropTypes.func,
  onBlur: PropTypes.func,
  hasError: PropTypes.bool,
  errorMessage: PropTypes.string,
  isMandatory: PropTypes.bool,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    // e.g costs
    PropTypes.number,
    PropTypes.string
  ]),
  error: PropTypes.string,
  pattern: PropTypes.string,
  maxLength: PropTypes.number,
  max: PropTypes.number,
  disabled: PropTypes.bool,
  autoFocus: PropTypes.bool,
  noCopy: PropTypes.bool,
  noPaste: PropTypes.bool
};

TextInput.defaultProps = {
  type: 'text',
  disabled: false,
  hasError: false
};

export default TextInput;
