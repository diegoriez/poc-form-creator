import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Dropdown as DropdownSUI, InputState } from 'react-shared-ui-next';
import _ from 'lodash';
import classNames from 'classnames';

const orderAscDesc = (xs = []) => _.orderBy(xs, ['key'], ['asc', 'desc']);
const sortIt = (isSort = false, xs = []) => (isSort ? orderAscDesc(xs) : xs);

class SelectBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  onKeyDownHandler(event) {
    if (this.state.hasFocus) {
      const keyCode = event.which || event.keyCode || event.charCode;
      // if <space> or <enter> key is pressed => toggle dropdown dialog
      if (keyCode === 13 || keyCode === 32) {
        event.preventDefault();
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
    }
  }

  render() {
    const {
    qaId,
    disabled,
    options,
    required,
    value,
    hasError,
    label,
    errorMessage,
    sort,
    onChange,
    } = this.props;

    let opts = options || [];



    opts = sortIt(sort, opts).map(opt => ({content: opt.key, value: opt.value}));

    return (
      <DropdownSUI
        qaId={qaId}
        className={classNames({ 'selectbox--error': hasError })}
        onClick={() => this.setState({ isOpen: !this.state.isOpen })}
        onFocus={() => this.setState({ hasFocus: true})}
        onBlur={() => this.setState({ hasFocus: false, isOpen: false })}
        onChange={val => { this.setState({ isOpen: false }); onChange(val); }}
        inputState={hasError ? InputState.ERROR : InputState.NONE}
        errorMessage={errorMessage}
        isOpen={this.state.isOpen}
        disabled={disabled}
        toggle={this.toggle}
        label={label}
        required={required}
        value={value}
        options={opts}
        onKeyDown={(e) => this.onKeyDownHandler(e)}
      />
    );
  }
}

SelectBox.propTypes = {
  qaId: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  className: PropTypes.string,
  options: PropTypes.array,
  required: PropTypes.bool,
  label: PropTypes.string,
  value: PropTypes.string,
  name: PropTypes.string,
  autoFocus: PropTypes.bool,
  hasError: PropTypes.bool,
  errorMessage: PropTypes.string,
  sort: PropTypes.bool,
};

export default SelectBox;

